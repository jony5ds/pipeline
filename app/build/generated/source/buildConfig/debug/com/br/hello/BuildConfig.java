/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.br.hello;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.br.hello";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "1.0";
  // Field from build type: debug
  public static final String BASE_URL = "https://google.com";
}
