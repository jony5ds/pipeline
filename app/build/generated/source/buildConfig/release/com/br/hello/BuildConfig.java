/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.br.hello;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.br.hello";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 29;
  public static final String VERSION_NAME = "29.0";
  // Field from build type: release
  public static final String BASE_URL = "ULR-LOCAL";
}
